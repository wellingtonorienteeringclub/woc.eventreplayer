﻿using System;
using System.Linq;
using System.Threading;
using IOF.XML.V3;

namespace WOC.EventReplayer {
  public class ReplayWorker {
    public void DoWork(Configuration configuration) {
      var resultsList = AdjustStartTimes(XmlSerializerHelper.DeserilizeFile<ResultList>(configuration.SourceXml), configuration.ZeroHour);
      var results = resultsList.ClassResult.SelectMany(p => p.PersonResult).Where(p => p.Result != null && p.Result.Length > 0).Select(p => p.Result[0]);
      var starts = results.Where(p => p.StartTimeSpecified);
      var firstStart = starts.Min(p => p.StartTime);
      var lastStart = starts.Max(p => p.StartTime);
      var startTime = (new DateTime(firstStart.Year, firstStart.Month, firstStart.Day, firstStart.Hour, firstStart.Minute, 0)).AddMinutes(-configuration.RegistrationBeforeStartInMinutes).AddMinutes(configuration.StartAdjustInMinutes);
      var lastFinish = results.Where(p => p.FinishTimeSpecified).Select(p => p.FinishTime).Max();
      var lastFailedToFinishStartAll = results.Where(p => !p.FinishTimeSpecified && p.StartTimeSpecified).Select(p => p.StartTime).ToList();
      var lastFailedToFinishStart = lastFailedToFinishStartAll.Count > 0 ? lastFailedToFinishStartAll.Max() : (DateTime?)null;
      var lastFailedToFinishUpdate = lastFailedToFinishStart.HasValue ? lastFailedToFinishStart.Value.AddMinutes(configuration.FailedTimeToShowInMinutes) : (DateTime?)null;
      var endTime = lastFailedToFinishUpdate.HasValue ? lastFinish > lastFailedToFinishUpdate ? lastFinish : lastFailedToFinishUpdate : lastFinish;

      Console.WriteLine($"First start: {firstStart}");
      Console.WriteLine($"Last start: {lastStart}");
      Console.WriteLine($"Last failed to finish start: {lastFailedToFinishStart}");
      Console.WriteLine($"Last finished: {lastFinish}");

      var processedXml = XmlSerializerHelper.SerilizeToString(resultsList);

      bool doContinue = true;

      while (doContinue) {
        resultsList = XmlSerializerHelper.DeserilizeString<ResultList>(processedXml);
        int total = 0;

        foreach (var item in resultsList.ClassResult) {
          item.PersonResult = item.PersonResult
            .Where(p => p.Result != null && p.Result.Length > 0 && p.Result[0].StartTimeSpecified && p.Result[0].StartTime.AddMinutes(-configuration.RegistrationBeforeStartInMinutes) < startTime)
            .ToArray();

          total += item.PersonResult.Count();

          foreach (var participant in item.PersonResult) {
            if (participant.Result[0].FinishTimeSpecified) {
              if (participant.Result[0].FinishTime < startTime) {
                //Finished do nothing.
              } else {
                participant.Result[0].FinishTimeSpecified = false;
                participant.Result[0].TimeSpecified = false;
                participant.Result[0].Status = ResultStatus.Active;
                participant.Result[0].Position = "";
              }
            } else {
              if (participant.Result[0].StartTime.AddMinutes(configuration.FailedTimeToShowInMinutes) < startTime) {
                //Show failed status 30 min after they started.
              } else {
                participant.Result[0].TimeSpecified = false;
                participant.Result[0].Status = ResultStatus.Active;
                participant.Result[0].Position = "";
              }
            }
          }

          item.PersonResult = item.PersonResult.OrderBy(p => p, new PositionComparer()).ToArray();

          int position = 0;
          double previousTime = -1;
          int postionOffSet = 0;

          foreach (var participant in item.PersonResult) {
            if (participant.Result.Length > 0 && participant.Result[0].Status == ResultStatus.OK && participant.Result[0].TimeSpecified) {
              if (participant.Result[0].Time == previousTime) {
                postionOffSet++;
              } else {
                position = position + postionOffSet + 1;
                postionOffSet = 0;
              }

              participant.Result[0].Position = position.ToString().PadLeft(2, '0');
            }
          }
        }

        Console.WriteLine($"Saving {total} at {startTime.Hour}:{startTime.Minute.ToString().PadLeft(2, '0')}");
        Retry.Do(() => XmlSerializerHelper.SerilizeToFile(configuration.DestinationXml, resultsList), TimeSpan.FromMilliseconds(100), 10);

        Thread.Sleep(TimeSpan.FromSeconds(1));

        if (startTime > endTime) {
          doContinue = false;
        }

        // Start time need to update after check to make sure that the last runner is shown.
        startTime = startTime.AddSeconds(configuration.StepInSeconds);
      }

      Console.WriteLine("Done!");
    }

    private ResultList AdjustStartTimes(ResultList resultsList, int zeroHour) {
      foreach (var c in resultsList.ClassResult) {
        foreach (var p in c.PersonResult) {
          foreach (var r in p.Result) {
            if (r.StartTimeSpecified && r.StartTime.Hour < zeroHour) {
              r.StartTime = r.StartTime.AddHours(12);
            }
          }
        }
      }

      return resultsList;
    }
  }
}