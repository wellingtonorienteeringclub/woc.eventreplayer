﻿namespace WOC.EventReplayer {
  public class Configuration {
    public int ZeroHour { get; set; }
    public int RegistrationBeforeStartInMinutes { get; set; }
    public int FailedTimeToShowInMinutes { get; set; }
    public string SourceXml { get; set; }
    public string DestinationXml { get; set; }
    public int StepInSeconds { get; set; }
    public int StartAdjustInMinutes { get; set; }
  }
}