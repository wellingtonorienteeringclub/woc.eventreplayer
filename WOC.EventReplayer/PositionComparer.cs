﻿using System.Collections.Generic;
using IOF.XML.V3;

namespace WOC.EventReplayer {
  public class PositionComparer : IComparer<PersonResult> {
    public int Compare(PersonResult x, PersonResult y) {
      if (x.Result[0].Status == y.Result[0].Status) {
        if (x.Result[0].Status == ResultStatus.OK) {
          return x.Result[0].Time.CompareTo(y.Result[0].Time);
        }

        return (x.Person.Name.Given + " " + x.Person.Name.Family).CompareTo(y.Person.Name.Given + " " + y.Person.Name.Family);
      }

      if (x.Result[0].Status == ResultStatus.Active) {
        return 1;
      }

      if (y.Result[0].Status == ResultStatus.Active) {
        return -1;
      }

      return 0;
    }
  }
}