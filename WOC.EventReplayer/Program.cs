﻿using System.IO;
using Newtonsoft.Json;

namespace WOC.EventReplayer {
  class Program {
    static void Main(string[] args) {
      var path = "replay.json";

      if (!File.Exists(path)) {
        File.WriteAllText(path, JsonConvert.SerializeObject(new Configuration {
          DestinationXml = "c:\\temp\\out.xml",
          FailedTimeToShowInMinutes = 30,
          RegistrationBeforeStartInMinutes = 5,
          SourceXml = "c:\\temp\\in.xml",
          StepInSeconds = 6,
          ZeroHour = 8,
          StartAdjustInMinutes = 0
        }));
      }

      var config = JsonConvert.DeserializeObject<Configuration>(File.ReadAllText(path));

      new ReplayWorker().DoWork(config);
    }
  }
}