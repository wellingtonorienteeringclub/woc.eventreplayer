﻿using System.IO;
using System.Xml.Serialization;

namespace WOC.EventReplayer {
  public class XmlSerializerHelper {
    public static T DeserilizeFile<T>(string filePath) where T : class {
      using (var stream = File.OpenRead(filePath)) {
        return new XmlSerializer(typeof(T)).Deserialize(stream) as T;
      }
    }

    public static T DeserilizeString<T>(string xml) where T : class {
      using (var stringReader = new StringReader(xml)) {
        return new XmlSerializer(typeof(T)).Deserialize(stringReader) as T;
      }
    }

    public static void SerilizeToFile<T>(string path, T instance) where T : class {
      using (var stream = File.Open(path, FileMode.Create)) {
        new XmlSerializer(typeof(T)).Serialize(stream, instance);
      }
    }

    public static string SerilizeToString<T>(T instance) where T : class {
      using (var stringwriter = new StringWriter()) {
        new XmlSerializer(typeof(T)).Serialize(stringwriter, instance);
        return stringwriter.ToString();
      }
    }
  }
}